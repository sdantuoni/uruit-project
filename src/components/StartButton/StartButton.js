import React from 'react';
import animate from 'gsap';
import PropTypes from 'prop-types';

class StartButton extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const props = this.props;

    return (
     <div className="StartButton">
       <button disabled={props.disabled} onClick={props.startGame}>{props.text}</button>
     </div>
    );
  }
}

StartButton.propTypes = {
  text: PropTypes.string,
  disabled: PropTypes.bool
};

StartButton.defaultProps = {
  text: 'Start Game',
  disabled: false
};

export default StartButton;
