import React from 'react';
import PropTypes from 'prop-types';

class Score extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      winners: []
    };
  }
  
  componentWillReceiveProps() {
    const prop = this.props.winners;
    let winners = prop.slice();
    this.setState({ winners });
  }

  render() {
    const props = this.props;
    const state = this.state;

    return (
      <div className="Score">
         <table>
          <tbody>
            <tr>
              <th>Round</th>
              <th>Winner</th> 
            </tr>
              {state.winners.map((winner, key) => 
                <tr key={key}>
                  <td>{winner.round}</td>
                  <td>{winner.name}</td> 
                </tr>
              )}
            </tbody>
          </table>
      </div>
    );
  }
}

Score.propTypes = {
  text: PropTypes.string,
  disabled: PropTypes.bool
};

Score.defaultProps = {
  text: 'Start Game',
  disabled: false
};

export default Score;
