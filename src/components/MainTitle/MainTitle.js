import React from 'react';
import PropTypes from 'prop-types';

class MainTitle extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    const props = this.props;

    return (
     <div className="MainTitle">
        <h2 style={props.style}>{props.title}</h2>
     </div>
    );
  }
}

MainTitle.propTypes = {
  title: PropTypes.string,
  style: PropTypes.object
};

MainTitle.defaultProps = {
  title: 'Set Any Title Here',
};

export default MainTitle;
