import React from 'react';
import PropTypes from 'prop-types';

class PlayerNames extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      player0: '',
      player1: ''
    }
  }
  
  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
        [name]: value
      });
      
  }

  componentDidUpdate() {
    if(this.state.player0.length >= 3 && this.state.player1.length >= 3){
      this.props.disableStart(false);           
    }else{
      this.props.disableStart(true);      
    }
  }

  getReadyPlayers() {
    return this.state;
  }

  render() {
    const props = this.props;

    return (
     <div className="PlayerNames">
      {props.players.map((player, key) =>
          <div className="player-container" key={key}>
            <label className="player-label">{player.name}</label>
            <input name={`player${key}`} className="player-input" placeholder={player.placeholder} onChange={(e) => this.handleInputChange(e)} />
          </div>
        )}
     </div>
    );
  }
}

PlayerNames.propTypes = {
  players: PropTypes.array.isRequired,
};

export default PlayerNames;
