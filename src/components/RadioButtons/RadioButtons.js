import React from 'react';
import PropTypes from 'prop-types';
import SVGInline from 'react-svg-inline';


class RadioButtons extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      current: ''
    }
  }

  onSelect(selected) {
    this.setState({ current: selected });
    this.props.onSelect(selected);
  }

  render() {
    const props = this.props;

    return (
     <div className="RadioButtons">
          <div className="radio-tile-group">
            {
              props.movesList.map((move, key) =>
              <div className="input-container" key={key}>
                <input id={move.name.toLowerCase()} className="radio-button" type="radio" name="radio" 
                    onChange={(e) => this.onSelect(e.target.id)} 
                    checked={props.defaultSelected === move.name.toLowerCase() ? true : false} />
                <div className="radio-tile">
                  <div className="icon">
                    <SVGInline
                        className="option-icon"
                        svg={move.icon}
                        component="div"
                      />
                  </div>
                  <label htmlFor={move.name.toLowerCase()} className="radio-tile-label">{move.name}</label>
                </div>
              </div>
              )
            }
        </div>
     </div>
    );
  }
}

RadioButtons.propTypes = {
  movesList: PropTypes.array.isRequired
};

export default RadioButtons;
