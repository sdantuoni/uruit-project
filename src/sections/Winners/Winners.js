import React from 'react';
import animate from 'gsap';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router'
import store from '../../store';
import MainTitle from '../../components/MainTitle/MainTitle';


class Winner extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      redirectToHome: false,
      name: ''
    }
  }

  componentDidMount() {
    animate.set(this.container, {autoAlpha: 0});
    if(store.getState().winners){
      this.setState({ name: store.getState().winners[0].name });
    }else{
      this.setState({ redirectToHome: true });
    }
  }

  componentWillAppear(done) {
    this.animateIn(done);
  }

  componentWillEnter(done) {
    this.animateIn(done);
  }

  componentWillLeave(done) {
    this.animateOut(done);
  }

  animateIn = (onComplete) => {
    animate.to(this.container, 0.5, {autoAlpha: 1, onComplete});
  };

  animateOut = (onComplete) => {
    animate.to(this.container, 0.5, {autoAlpha: 0, onComplete});
  };



  render() {
    const props = this.props;
    const state = this.state;
    const style = Object.assign({}, props.style);

    return (
      <main
        id="Winner"
        style={style}
        ref={r => this.container = r}
      >
        {state.redirectToHome && <Redirect to="/" />}
        <MainTitle style={{ textAlign: 'center' }} title="We have a WINNER!!" />
        <MainTitle style={{ textAlign: 'center' }} title={`${this.state.name} is the new EMPEROR!`} />
      </main>
    );
  }
}

Winner.propTypes = {
  style: PropTypes.object,
};

Winner.defaultProps = {
  style: {},
};

export default Winner;

