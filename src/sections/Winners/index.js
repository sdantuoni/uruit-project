import { connect } from 'react-redux';
import connectTransitionWrapper from '../../decorators/connectTransitionWrapper';
import Winners from './Winners';
// import { addPlayer } from '../../store/actions/landing';


const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    addPlayer: val => dispatch(addPlayer(val)),
  };
};

@connectTransitionWrapper()
@connect(
  mapStateToProps,
  mapDispatchToProps,
  undefined,
  {withRef: true}
)

export default class WinnersWrapper extends Winners {
  constructor(props) {
    super(props);
  }
}
