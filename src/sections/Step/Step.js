import React from 'react';
import animate from 'gsap';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router'

import MainTitle from '../../components/MainTitle/MainTitle';
import RadioButtons from '../../components/RadioButtons/RadioButtons';
import store from '../../store';
import StartButton from '../../components/StartButton/StartButton';
import Score from '../../components/Score/Score';


import Paper from '../../../raw-assets/svg/paper.svg'
import Scissors from '../../../raw-assets/svg/scissors.svg'
import Rock from '../../../raw-assets/svg/rock.svg'


const movesList = [
    {
        name: 'Rock',
        icon: Rock
    },
    {
       name: 'Paper',
       icon: Paper
    },
    {
        name: 'Scissors',
        icon: Scissors
    }
];

const movesWins = [
  { move: 'paper', kills: 'rock'},
  { move: 'rock', kills: 'scissors'},
  { move: 'scissors', kills: 'paper'},
];


class Step extends React.PureComponent {
  constructor(props) {
    super(props);
    this.child = React.createRef();
    this.state = {
      round: 1,
      readyPlayers: [],
      redirectToHome: false,
      redirectToWinners: false,
      currentPlayer: 0,
      moves: [],
      resetRadio: false,
      move: '',
      winners: []
    }
  }

  componentDidMount() {
    animate.set(this.container, {autoAlpha: 0});
    this.storePlayers();
  }

  componentWillAppear(done) {
    this.animateIn(done);
  }

  componentWillEnter(done) {
    this.animateIn(done);
  }

  componentWillLeave(done) {
    this.animateOut(done);
  }

  animateIn = (onComplete) => {
    animate.to(this.container, 0.5, {autoAlpha: 1, onComplete});
  };

  animateOut = (onComplete) => {
    animate.to(this.container, 0.5, {autoAlpha: 0, onComplete});
  };

  storePlayers() {
    let players = store.getState().players[0];
    if(players) {
      this.setState({ readyPlayers: players });
    }else{
      this.setState({ redirectToHome: true });
    }
  }

  handleSelect(move) {
    const readyPlayers = this.state.readyPlayers; 
    readyPlayers[this.state.currentPlayer].move = move;
    this.setState({ move: move });
    this.setState(readyPlayers);
  }

  setWin(player) {
    let state = this.state;
    let readyPlayers = state.readyPlayers; 
    let round = state.round + 1;
    readyPlayers[player].wins = readyPlayers[player].wins + 1;

    let winners = state.winners;
    winners.push({
      name:  readyPlayers[player].name,
      round: state.round
    });
    this.setState({ winners });
    
    if(state.round === 3 && state.currentPlayer === 1){
        let p1 = state.readyPlayers[0].wins;
        let p2 = state.readyPlayers[1].wins;
        let winners = [];
        if(p1 > p2){
          winners.push({
            name:  readyPlayers[0].name,
          });
          this.setState({ redirectToWinners: true });
        }else if(p1 < p2) {
          winners.push({
            name:  readyPlayers[1].name,
          });
          this.setState({ redirectToWinners: true });
        }else {
          this.setState({ redirectToHome: true });
        }
        this.props.setWinners(winners);
    }else{ 
      this.setState(readyPlayers);
      this.setState({ currentPlayer: 0 });
      this.setState({ move: '' });
      this.setState({ round });

    }
  }

  spinMoves() {
    var _this = this;
    function spin (){
      switch(_this.state.move){
        case '':
          _this.setState({ move: 'rock' });
          break;

        case 'rock':
          _this.setState({ move: 'paper' });
          break;

        case 'paper':
          _this.setState({ move: 'scissors' });
          break;

        case 'scissors':
          _this.setState({ move: '' });
          break;

      }
    }

    var x = 4;
    var interval = 600;
    
    for (var i = 0; i < x; i++) {
        setTimeout(function () {
           spin();
        }, i * interval)
    }
  }

    playRound() {
      const state = this.state;
      if(state.move === undefined || state.move === ''){
        this.spinMoves();
        return;
      }
      if(state.round <= 3){
        if(state.currentPlayer === 0){
          this.setState({ currentPlayer: 1 });
          this.setState({ move: '' });
        }else if(state.currentPlayer === 1){
            let p1 = state.readyPlayers[0].move;
            let p2 = state.readyPlayers[1].move;
    
            if(p1 === p2){
              console.log("You can't select the same move of your opponent"); 
              this.setState({ currentPlayer: 0 });
              this.setState({ move: '' });      
            }else{
              movesWins.forEach(win => {
                if(p1 === win.move){
                  if(p2 === win.kills){
                    this.setWin(0);
                  }else{
                    this.setWin(1);
                  }
                }
              });
            }
        }
      }
  }
 
  render() {
    const props = this.props;
    const state = this.state;
    const style = Object.assign({}, props.style);
    const players = state.readyPlayers;

    // console.log(state.winners)

    return (
      <main
        id="Step"
        style={style}
        ref={r => this.container = r}
      >
        {state.redirectToHome && <Redirect to="/" />}
        {state.redirectToWinners && <Redirect to="/winners" />}
        <Score winners={state.winners} className={'score-table'} />
        <MainTitle style={{ marginTop: '10rem' }} title={`Round ${state.round}`} />
        {state.readyPlayers.length !== 0 &&  <MainTitle title={players[state.currentPlayer].name} style={{ fontSize: '3rem', marginTop: '5rem' }} />}
        <MainTitle style={{ fontSize: '2rem', marginTop: '4rem' }} title="Select Your Play" />
        <RadioButtons 
            movesList={movesList} 
            defaultSelected={state.move}
            onSelect={(move) => this.handleSelect(move)}
            />
        <StartButton startGame={() => this.playRound()} text="PLAY"/>
      </main>
    );
  }
}

Step.propTypes = {
  style: PropTypes.object,
};

Step.defaultProps = {
  style: {},
};

export default Step;

