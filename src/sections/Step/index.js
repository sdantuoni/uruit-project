import { connect } from 'react-redux';
import connectTransitionWrapper from '../../decorators/connectTransitionWrapper';
import Step from './Step';
import { setWinners } from '../../store/actions/step';



const mapStateToProps = (state, ownProps) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    setWinners: val => dispatch(setWinners(val)),
  };
};

@connectTransitionWrapper()
@connect(
  mapStateToProps,
  mapDispatchToProps,
  undefined,
  {withRef: true}
)

export default class StepWrapper extends Step {
  constructor(props) {
    super(props);
  }
}
