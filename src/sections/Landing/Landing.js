import React from 'react';
import animate from 'gsap';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router'

import MainTitle from '../../components/MainTitle/MainTitle';
import PlayerNames from '../../components/PlayerNames/PlayerNames';
import StartButton from '../../components/StartButton/StartButton';

class Landing extends React.PureComponent {
  constructor(props) {
    super(props);
    this.child = React.createRef();
    this.state = {
      players: [
        {
          name: 'Player 1',
          placeholder: 'Insert Your Name'
        },
        {
          name: 'Player 2',
          placeholder: 'Insert Your Name'
        }
      ],
      readyPlayers: [],
      redirect: false,
      disableStart: true
    }
  }

  componentDidMount() {
    animate.set(this.container, {autoAlpha: 0});
  }

  componentWillAppear(done) {
    this.animateIn(done);
  }

  componentWillEnter(done) {
    this.animateIn(done);
  }

  componentWillLeave(done) {
    this.animateOut(done);
  }

  animateIn = (onComplete) => {
    animate.to(this.container, 0.5, {autoAlpha: 1, onComplete});
  };

  animateOut = (onComplete) => {
    animate.to(this.container, 0.5, {autoAlpha: 0, onComplete});
  };

  disableStart = (option) => {
    this.setState({ disableStart: option });
  };

  startGame() {
    let readyPlayers = this.child.current.getReadyPlayers();
    const storePlayers = [
      {
        0: {
          name: readyPlayers.player0,
          wins: 0
        },
        1: {
          name: readyPlayers.player1,
          wins: 0
        }
      }
    ]
    this.props.addPlayer(storePlayers);
    this.setState({ redirect: true });
  }

  render() {
    const props = this.props;
    const style = Object.assign({}, props.style);

    return (
      <main
        id="Landing"
        style={style}
        ref={r => this.container = r}
      >
        <MainTitle title={"Enter Player's Names"} />
        <PlayerNames ref={this.child} players={this.state.players} disableStart={this.disableStart}/>
        <StartButton startGame={() => this.startGame()} disabled={this.state.disableStart} />
        {this.state.redirect && <Redirect to="/step" />}
      </main>
    );
  }
}

Landing.propTypes = {
  style: PropTypes.object,
  windowWidth: PropTypes.number,
  windowHeight: PropTypes.number,
  addPlayer: PropTypes.func.isRequired
};

Landing.defaultProps = {
  style: {},
};

export default Landing;

