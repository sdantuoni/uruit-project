/**
 * Components added to the manifest can be accessed in browser under `/test/{componentName}` route
 * or go to `/test` to see the list of test components
 */

const manifest = [
  {
    key: 'rotate',
    Component: require('../components/Rotate/Rotate').default,
    props: {
      portrait: true
    }
  },
  {
    key: 'preloader',
    Component: require('./components/Preloader').default,
    props: {
      minDisplayTime: 1500
    }
  },
  {
    key: 'sound',
    Component: require('./components/SoundTest').default,
    props: {}
  }
];

export default manifest
  .concat({
    key: 'test-index-page',
    Component: require('./components/TestPage').default,
    path: {
      path: '/test',
      exact: true
    }
  })
  .map(route => ({
    path: {
      path: '/test/' + route.key,
      exact: true,
    },
    ...route
  }));
