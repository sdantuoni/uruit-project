import Landing from '../sections/Landing';
import Step from '../sections/Step';
import Winners from '../sections/Winners';


export default [
  {
    key: 'landing',
    Component: Landing,
    path: {
      path: '/',
      exact: true
    }
  },
  {
    key: 'step',
    Component: Step,
    path: {
      path: '/step',
      exact: true
    }
  },
  {
    key: 'winners',
    Component: Winners,
    path: {
      path: '/winners',
      exact: true
    }
  }
];
