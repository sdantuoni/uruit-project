import keys from '../keys';

export const winners = function(state = false, action) {
  switch (action.type) {
    case keys.SET_WINNERS:
      return action.winners;
    default:
      return state;
  }
};