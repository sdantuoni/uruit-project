import keys from '../keys';

export const players = function(state = false, action) {
  switch (action.type) {
    case keys.PLAYER_DATA:
      return action.players;
    default:
      return state;
  }
};