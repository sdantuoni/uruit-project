import keys from '../keys';

export const addPlayer = function(players) {
  return {
    type: keys.PLAYER_DATA,
    players,
  };
};