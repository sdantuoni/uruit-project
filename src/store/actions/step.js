import keys from '../keys';

export const setWinners = function(winners) {
  return {
    type: keys.SET_WINNERS,
    winners,
  };
};